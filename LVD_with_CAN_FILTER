/*  ChibiOS - Copyright (C) 2006..2018 Giovanni Di Sirio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

  ///////// BMS REQUESTS ///////////
  char get_pack_voltage[] =  {0xAA, 0x14, 0x7F, 0x1F};
  char get_pack_current[] =  {0xAA, 0x15, 0xBE, 0xDF};
  char get_max_voltage[]  =  {0xAA, 0x16, 0xFE, 0xDE};
  char get_min_voltage[]  =  {0xAA, 0x17, 0x3F, 0x1E};
  char get_temperature[]  =  {0xAA, 0x1B, 0x3F, 0x1B};
  char reset_bms[]        =  {0xAA, 0x02, 0x05, 0x90, 0x83};

  // voltage, current, temperature globals to share between main
  volatile float temp_voltage; // variable to store voltage
  volatile float temp_temperature1; // variable to store the temperature of one of the cells
  volatile float temp_temperature2; // variable to store one of the temperatures in another cell
  volatile float temp_temperature3; // variable to store one of the temperatures in another cell
  volatile float temp_current; // variable to store current

  //////////////////////////////////

  ///////// CAN GLOBAL VARIABLES ///////////
  short volt_data;
  short curr_data;
  short mincell_data;
  short maxcell_data;
  int tx_ready;

  ///////// CONSTANT PARAMETERS ///////////
#define MAX_VOLTAGE 20.51
#define MAX_CURRENT 40.00
#define MAX_CELL_VOLTAGE 4
#define MIN_CELL_VOLTAGE 2
#define MAX_TEMPERATURE 80

#include "ch.h"
#include "hal.h"
#include "rt_test_root.h"
#include "oslib_test_root.h"
#include "chprintf.h"
#include <stdio.h>
#include <string.h>



  struct can_instance {
    CANDriver     *canp;
    uint32_t      led;
  };

  static const SerialConfig sd5config = {
    115200,
    NULL,
    NULL,
    NULL
  };

  static const SerialConfig sd6config = {
    38400,
    NULL,
    NULL,
    NULL
  };
  static const struct can_instance can1 = {&CAND1, 11};

  /*
   * Internal loopback mode (CAN_BTR_LBCK) (disabled), 500KBaud, automatic wakeup, automatic recover
   * from abort mode.
   * See section 22.7.7 on the STM32 reference manual.
   */
  static const CANConfig cancfg = {
    CAN_MCR_ABOM | CAN_MCR_AWUM | CAN_MCR_TXFP,
    CAN_BTR_SJW(0) | CAN_BTR_TS2(1) |
    CAN_BTR_TS1(8) | CAN_BTR_BRP(6)
  };

  /*
   * Receiver thread.
   */
  static THD_WORKING_AREA(can_rx1_wa, 256);
  static THD_FUNCTION(can_rx, p) {
    struct can_instance *cip = p;
    event_listener_t el;
    CANRxFrame rxmsg;

    (void)p;
    chRegSetThreadName("receiver");
    chEvtRegister(&cip->canp->rxfull_event, &el, 0);
    while (true) {
      if (chEvtWaitAnyTimeout(ALL_EVENTS, TIME_MS2I(100)) == 0)
        continue;
      while (canReceive(cip->canp, CAN_ANY_MAILBOX,
                        &rxmsg, TIME_IMMEDIATE) == MSG_OK) {
        /* Process message.*/

        palTogglePad(GPIOA, cip->led);
      }
    }
    chEvtUnregister(&CAND1.rxfull_event, &el);
  }

  /*
   * Transmitter thread.
   */
  static THD_WORKING_AREA(can_tx_wa, 256);
  static THD_FUNCTION(can_tx, p) {
    CANTxFrame txmsg;

    (void)p;
    chRegSetThreadName("transmitter");
    txmsg.IDE = CAN_IDE_STD;
    txmsg.SID = 0x000001A1; // can only represent 11 bits of ID
    txmsg.RTR = CAN_RTR_DATA;
    txmsg.DLC = 8;
    txmsg.data32[0] = 0x55AA55AA;
    txmsg.data32[1] = 0x00FF00FF;

    while (true) {
      if(tx_ready){
        txmsg.data16[0] = volt_data;
        txmsg.data16[1] = curr_data;
        txmsg.data16[2] = maxcell_data;
        txmsg.data16[3] = mincell_data;
        if(!canTransmit(&CAND1, CAN_ANY_MAILBOX, &txmsg, TIME_MS2I(100))) tx_ready = FALSE;
        else palTogglePad(GPIOA,11);
      }
      chThdSleepMilliseconds(50);
    }
  }


/*
 * This is a periodic thread that does absolutely nothing except flashing
 * a LED.
 */
static THD_WORKING_AREA(waThread1, 128);
static THD_FUNCTION(Thread1, arg) {

  (void)arg;
  chRegSetThreadName("blinker");
  chThdSleepMilliseconds(8000); // setup time for the bms to turn on properly
  while (true) {
    chThdSleepMilliseconds(500);

    // temporary values just to test out the system. Will need to test outside for actual values
    // and when we actually connect the other modules to the batteries
    if (temp_voltage <= 17.25 || temp_temperature >= 33.1 || temp_current >= 30.5){
      palSetPad(GPIOA, 0);
    }
    else
      palClearPad(GPIOA, 0);

    chThdSleepMilliseconds(500);
  }
}

 // Function Prototypes
void get_data(int id, char* buff, unsigned short* crc_extract);
unsigned int CRC16_2(char* buf, int len);
int GetString(char *s, int nmax);
void parse_data(int id, char* buff, unsigned long *data);
/*
 * Application entry point.
 */
int main(void) {

  /*
   * System initializations.
   * - HAL initialization, this also initializes the configured device drivers
   *   and performs the board-specific initializations.
   * - Kernel initialization, the main() function becomes a thread and the
   *   RTOS is active.
   */

  char buff[50];
  int sensor_id = 1;
  int crc_length;
  unsigned short crc_verify; // our crc value from our algorithm used to verify
  unsigned short crc_extract; // the extracted crc value from bms
  unsigned short temp_maxcell;
  unsigned short temp_mincell;
  unsigned long parsed_data;  // extracted data from the bms output once crc is verified

  halInit();
  chSysInit();

  /*
   * Activates the serial driver 2 using the driver default configuration.
   * PA2(TX) and PA3(RX) are routed to USAT2. Used to read/write to BMS
   */

  sdStart(&SD5, &sd5config);
  palSetPadMode(GPIOD, 2, PAL_MODE_ALTERNATE(8)); // sd5 tx
  palSetPadMode(GPIOC, 12, PAL_MODE_ALTERNATE(8)); // sd5 rx

  /*
   * Activates the serial driver 6 using the driver default configuration.
   * PC6(TX) and PC7(RX) are routed to USART6. Used to read/write to Computer
   */

  // Serial Driver Receive/Transmit
  sdStart(&SD6, &sd6config);
  palSetPadMode(GPIOC, 6, PAL_MODE_ALTERNATE(8)); //sd6 tx
  palSetPadMode(GPIOC, 7, PAL_MODE_ALTERNATE(8)); //sd6 rx

  // STM32 CAN input/output pins
  palSetPadMode(GPIOB, 8, PAL_MODE_ALTERNATE(9)); // CAN1 rx
  palSetPadMode(GPIOB, 9, PAL_MODE_ALTERNATE(9)); // CAN1 tx
  canStart(&CAND1, &cancfg);

  // Output LVD pins
  palSetPadMode(GPIOA, 0, PAL_MODE_OUTPUT_PUSHPULL);
  palSetPadMode(GPIOB, 7, PAL_MODE_OUTPUT_OPENDRAIN);
  palSetPadMode(GPIOA, 11, PAL_MODE_OUTPUT_PUSHPULL);

  /*
   * Starting the transmitter and receiver threads for CAN.
   */
  chThdCreateStatic(can_rx1_wa, sizeof(can_rx1_wa), NORMALPRIO + 7,
                    can_rx, (void *)&can1);
  chThdCreateStatic(can_tx_wa, sizeof(can_tx_wa), NORMALPRIO + 7,
                    can_tx, NULL);
  /*
   * Creates the example thread.
   */
  //chThdCreateStatic(waThread1, sizeof(waThread1), NORMALPRIO, Thread1, NULL);

  /*
   * Normal main() thread activity, in this demo it does nothing except
   * sleeping in a loop and check the button state.
   */
  palTogglePad(GPIOA,11);
  chprintf(&SD6,"main");
  chThdSleepMilliseconds(500);
  palTogglePad(GPIOA,11);
  palClearPad(GPIOB,7);

  // making the filter for the LVD
  static const CANFilter lvdFilter[2] = {
  // Assign Filter 0 to 16 bit mask mode
  // Filter 0, mode 0 =mask, scale 0 = 16 bit mode, ass = 0, {r1}, {r2}
  {0, 0, 0, 0, 0x22204320, 0x00002460},
  // Assign Filter 1 to 16 bit identifier mode
  // Filter 1, mode 0 =mask, scale 0 = 16 bit mode, ass = 0, {r1}, {r2}
  {1, 1, 0, 0, 0x00000000, 0x00000000}
  };

  // setting the filters to on for the filter on CAN 1
    canSTM32SetFilters(&CAND1, 0x0, 2, lvdFilter);

  //code to reset the BMS once before the main loop for the current sensor to work properly
  if ((sdReadTimeout(&SD5,buff,4,500) != 5)){
    sdWrite(&SD5,&reset_bms, 5);
    chThdSleepMilliseconds(500);
  }

  while (true) {
    /*if (palReadPad(GPIOA, GPIOA_BUTTON)) {*/

      ////// resets the sensor id and sets the crc_length and cycles through all requests (voltage, temperature, current, etc)
      if (sensor_id > 5)
      {
      chprintf(&SD6,"%f,%f,%d,%d",(temp_voltage),(temp_current),volt_data,curr_data);
      volt_data = (short)((temp_voltage)*100);
      curr_data = (short)((temp_current)*100);
      maxcell_data = temp_maxcell;
      mincell_data = temp_mincell;
      tx_ready = 1;
      sensor_id = 1;
      }

      if (sensor_id == 1 || sensor_id == 2){
        crc_length = 6;
      }
      else if(sensor_id == 3 || sensor_id ==4){
        crc_length = 4;
      }
      else if(sensor_id == 5){
        crc_length = 9;
      }

      // Sending and Reading commands from TinyBMS
      get_data(sensor_id,buff, &crc_extract);

      // CRCing the input bytes
      crc_verify = CRC16_2(buff,crc_length);

      // If CRC is right then we extract and send the data
     if (crc_verify == crc_extract)
      {
       parse_data(sensor_id,buff,&parsed_data); //useless function that concatenates useful data from buffer into parsed_data, could replace with memcpy
        if (sensor_id == 1)
         {
          memcpy(&temp_voltage,&parsed_data,4);
          if ((temp_voltage) < MAX_VOLTAGE)
           {
            //sdWrite(&SD6, temp_voltage, 4);

            chprintf(&SD6, "The voltage is %f \r\n", temp_voltage);
           // chprintf(&SD6, "the integer voltage is %d \r\n",(int) ((temp_voltage)*100));
           }
         }
        else if (sensor_id == 2)
        {
          memcpy(&temp_current,&parsed_data,4);
          if (temp_current < MAX_CURRENT)
          {
            chprintf(&SD6,"The current is %f \r\n", temp_current);
            //sdWrite(&SD6, temp_current, 4);
          }
        }
        else if ( sensor_id == 3)
        {
          temp_maxcell = parsed_data;
          chprintf(&SD6,"The max cell voltage is %d \r\n", temp_maxcell);
          //sdWrite(&SD6, temp_maxcell, 2);
        }
        else if ( sensor_id == 4)
        {
          temp_mincell = parsed_data;
          chprintf(&SD6, "The min cell voltage is %d \r\n", temp_mincell);

        }
        else if ( sensor_id == 5)
        {
          memcpy(&temp_temperature1,buff+3,2);
          memcpy(&temp_temperature2,buff+5,2);
          memcpy(&temp_temperature3,buff+7,2);
          chprintf(&SD6, "BMS_TEMP 1 is %f \r\n", temp_temperature1);
          chprintf(&SD6, "BMS_TEMP 2 is %f \r\n", temp_temperature2);
          chprintf(&SD6, "BMS_TEMP 3 is %f \r\n", temp_temperature3);
        }

      }
     //what should happen if crc does something wrong

     //increment the sensor id to let it iterate
     sensor_id++;
     chprintf(&SD6,"main loop 3\r\n");
         chThdSleepMilliseconds(200);

    }

    halPolledDelay(15000);
    //sleep(10000);

  }

  unsigned int CRC16_2(char* buf, int len){
    unsigned int crc = 0xFFFF;
    for (int pos = 0; pos < len; pos++)
    {
    crc ^= (unsigned int)buf[pos];    // XOR byte into least sig. byte of crc

    for (int i = 8; i != 0; i--) {    // Loop over each bit
      if ((crc & 0x0001) != 0) {      // If the LSB is set
        crc >>= 1;                    // Shift right and XOR 0xA001
        crc ^= 0xA001;
      }
      else                            // Else LSB is not set
        crc >>= 1;                    // Just shift right
      }
    }
    return crc;
  }


void parse_data(int id, char* buff, unsigned long *data){
  int i;
  int lower_byte;
  int upper_byte;
  if (id == 1 || id == 2){
    lower_byte = 2;
    upper_byte = 5;
  }
  if (id ==3 || id ==4){
    lower_byte = 2;
    upper_byte = 3;
  }
  if (id == 5){
      lower_byte = 3;
      upper_byte = 8;
    }
  for (i = upper_byte; i >= lower_byte; i--){
      *data = *data <<8;
      *data = *data | buff[i];
         /*pack_voltage = pack_voltage | buff[4];
         pack_voltage = pack_voltage <<8;
         pack_voltage = pack_voltage | buff[3];
         pack_voltage = pack_voltage <<8;
         pack_voltage = pack_voltage | buff[2];
         */
     }
}

void get_data(int id, char* buff, unsigned short* crc_extract){
  char* temp_command;
  int read_size;
  int send_size;

  if (id == 1){
    temp_command = &get_pack_voltage;
    send_size = 4;
    read_size = 8;
  }
  if (id == 2){
    temp_command = &get_pack_current;
    send_size = 4;
    read_size = 8;
  }
  if (id == 3){
    temp_command = &get_max_voltage;
    send_size = 4;
    read_size = 6;
  }
  if (id == 4){
    temp_command = &get_min_voltage;
    send_size = 4;
    read_size = 6;
  }
  if (id == 5){
    temp_command = &get_temperature;
    send_size = 4;
    read_size = 11;
  }
  else


  sdWrite(&SD5, temp_command, send_size);
  sdReadTimeout(&SD5, buff, read_size, 100);
  //sdWrite(&SD6, buff, read_size);

  // Extracting CRC bytes from output to a variable
  *crc_extract = buff[read_size -1];
  *crc_extract = *crc_extract<<8;
  *crc_extract = *crc_extract | buff[read_size -2];


}

//}
//  int GetString(char * s, int nmax)
//  {
//      unsigned char c;
//      int n = 0;
//      while(1)
//      {
//
//          if( (c=='\n') || (c=='\r') || n==(nmax-1) )
//          {
//              *s=0;
//              return n;
//          }
//          else
//          {
//              *s=c;
//              s++;
//              n++;
//          }
//      }
//  }
