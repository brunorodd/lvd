/*  ChibiOS - Copyright (C) 2006..2018 Giovanni Di Sirio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

  ///////// BMS REQUESTS ///////////
  char get_pack_voltage[] =  {0xAA, 0x14, 0x7F, 0x1F};
  char get_pack_current[] =  {0xAA, 0x15, 0xBE, 0xDF};
  char get_max_voltage[]  =  {0xAA, 0x16, 0xFE, 0xDE};
  char get_min_voltage[]  =  {0xAA, 0x17, 0x3F, 0x1E};
  char get_temperature[]  =  {0xAA, 0x1B, 0x3F, 0x1B};
  //////////////////////////////////

#include "ch.h"
#include "hal.h"
#include "rt_test_root.h"
#include "oslib_test_root.h"
#include "chprintf.h"

/*
 * This is a periodic thread that does absolutely nothing except flashing
 * a LED.
 */
static THD_WORKING_AREA(waThread1, 128);
static THD_FUNCTION(Thread1, arg) {

  (void)arg;
  chRegSetThreadName("blinker");
  while (true) {
    palSetPad(GPIOD, GPIOD_LED4);       /* Orange.  */
    palClearPad(GPIOD, GPIOD_LED5);     /* Orange.  */
    chThdSleepMilliseconds(500);
    palClearPad(GPIOD, GPIOD_LED4);     /* Orange.  */
    palSetPad(GPIOD, GPIOD_LED5);       /* Orange.  */
    chThdSleepMilliseconds(500);
  }
}
unsigned int CRC16_2(char* buf, int len);
int GetString(char *s, int nmax);
/*
 * Application entry point.
 */
int main(void) {

  /*
   * System initializations.
   * - HAL initialization, this also initializes the configured device drivers
   *   and performs the board-specific initializations.
   * - Kernel initialization, the main() function becomes a thread and the
   *   RTOS is active.
   */
  //char bleh[] = "abcdef 3 ghijk \n \r";
  char get_voltage[] = {0xAA,0x14,0x7F,0x1F};
  char buff[50];
  int length;
  unsigned short crc_verify;
  unsigned short crc_extract;
  unsigned short data_extract;
  halInit();
  chSysInit();

  /*
   * Activates the serial driver 2 using the driver default configuration.
   * PA2(TX) and PA3(RX) are routed to USAT2.
   */
  sdStart(&SD2, NULL);
  palSetPadMode(GPIOA, 2, PAL_MODE_ALTERNATE(7)); // sd2 tx
  palSetPadMode(GPIOA, 3, PAL_MODE_ALTERNATE(7)); // sd2 rx

  sdStart(&SD1, NULL);
  palSetPadMode(GPIOB, 6, PAL_MODE_ALTERNATE(7)); //sd1 tx
  palSetPadMode(GPIOB, 7, PAL_MODE_ALTERNATE(7)); //sd1 rx

  /*
   * Creates the example thread.
   */
  chThdCreateStatic(waThread1, sizeof(waThread1), NORMALPRIO, Thread1, NULL);

  /*
   * Normal main() thread activity, in this demo it does nothing except
   * sleeping in a loop and check the button state.
   */
  while (true) {
    if (palReadPad(GPIOA, GPIOA_BUTTON)) {

      sdWriteTimeout(&SD2, get_max_voltage, 4 ,5);

      sdReadTimeout(&SD2, buff, 6, 10);

      sdWrite(&SD1, buff, 6);

      crc_verify = CRC16_2(buff, 4);
      sdWrite(&SD1, &crc_verify, 2);

      // Extracting CRC bytes from output to a variable
      crc_extract = buff[5];
      crc_extract = crc_extract<<8;
      crc_extract = crc_extract | buff[4];

      sdWrite(&SD1, &crc_extract,2);

     if (crc_verify == crc_extract)
      {
        chprintf(&SD1, "gud");
        data_extract = buff[3];
        data_extract = data_extract<<8;
        data_extract = data_extract | buff[2];
        sdWrite(&SD1, &data_extract, 2);
      }


    }

    chThdSleepMilliseconds(100);

  }
}
  int GetString(char * s, int nmax)
  {
      unsigned char c;
      int n = 0;
      while(1)
      {
          c=sdGet(&SD2);
          if( (c=='\n') || (c=='\r') || n==(nmax-1) )
          {
              *s=0;
              return n;
          }
          else
          {
              *s=c;
              s++;
              n++;
          }
      }
  }

  unsigned int CRC16_2(char* buf, int len){
    unsigned int crc = 0xFFFF;
    unsigned int temp;
    for (int pos = 0; pos < len; pos++)
    {
    crc ^= (unsigned int)buf[pos];    // XOR byte into least sig. byte of crc

    for (int i = 8; i != 0; i--) {    // Loop over each bit
      if ((crc & 0x0001) != 0) {      // If the LSB is set
        crc >>= 1;                    // Shift right and XOR 0xA001
        crc ^= 0xA001;
      }
      else                            // Else LSB is not set
        crc >>= 1;                    // Just shift right
      }
    }
    return crc;
  }
