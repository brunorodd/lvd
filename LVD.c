/*  ChibiOS - Copyright (C) 2006..2018 Giovanni Di Sirio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

  ///////// BMS REQUESTS ///////////
  char get_pack_voltage[] =  {0xAA, 0x14, 0x7F, 0x1F};
  char get_pack_current[] =  {0xAA, 0x15, 0xBE, 0xDF};
  char get_max_voltage[]  =  {0xAA, 0x16, 0xFE, 0xDE};
  char get_min_voltage[]  =  {0xAA, 0x17, 0x3F, 0x1E};
  char get_temperature[]  =  {0xAA, 0x1B, 0x3F, 0x1B};
  //////////////////////////////////

  ///////// CONSTANT PARAMETERS ///////////
#define MAX_VOLTAGE 20.51
#define MAX_CURRENT 40.00
#define MAX_CELL_VOLTAGE 4
#define MIN_CELL_VOLTAGE 2
#define MAX_TEMPERATURE 80

#include "ch.h"
#include "hal.h"
#include "rt_test_root.h"
#include "oslib_test_root.h"
#include "chprintf.h"
#include <stdio.h>
  struct can_instance {
    CANDriver     *canp;
    uint32_t      led;
  };

  static const struct can_instance can1 = {&CAND1, GPIOD_LED5};

  /*
   * Internal loopback mode, 500KBaud, automatic wakeup, automatic recover
   * from abort mode.
   * See section 22.7.7 on the STM32 reference manual.
   */
  static const CANConfig cancfg = {
    CAN_MCR_ABOM | CAN_MCR_AWUM | CAN_MCR_TXFP,
    CAN_BTR_LBKM | CAN_BTR_SJW(0) | CAN_BTR_TS2(1) |
    CAN_BTR_TS1(8) | CAN_BTR_BRP(6)
  };

  /*
   * Receiver thread.
   */
  static THD_WORKING_AREA(can_rx1_wa, 256);
  static THD_FUNCTION(can_rx, p) {
    struct can_instance *cip = p;
    event_listener_t el;
    CANRxFrame rxmsg;

    (void)p;
    chRegSetThreadName("receiver");
    chEvtRegister(&cip->canp->rxfull_event, &el, 0);
    while (true) {
      if (chEvtWaitAnyTimeout(ALL_EVENTS, TIME_MS2I(100)) == 0)
        continue;
      while (canReceive(cip->canp, CAN_ANY_MAILBOX,
                        &rxmsg, TIME_IMMEDIATE) == MSG_OK) {
        /* Process message.*/
        palTogglePad(GPIOD, cip->led);
      }
    }
    chEvtUnregister(&CAND1.rxfull_event, &el);
  }

  /*
   * Transmitter thread.
   */
  static THD_WORKING_AREA(can_tx_wa, 256);
  static THD_FUNCTION(can_tx, p) {
    CANTxFrame txmsg;

    (void)p;
    chRegSetThreadName("transmitter");
    txmsg.IDE = CAN_IDE_STD;
    txmsg.SID = 0x000;
    txmsg.RTR = CAN_RTR_DATA;
    txmsg.DLC = 8;
    txmsg.data32[0] = 0x55AA55AA;
    txmsg.data32[1] = 0x00FF00FF;

    while (true) {
      canTransmit(&CAND1, CAN_ANY_MAILBOX, &txmsg, TIME_MS2I(100));
      //canTransmit(&CAND2, CAN_ANY_MAILBOX, &txmsg, TIME_MS2I(100));
      chThdSleepMilliseconds(500);
    }
  }


/*
 * This is a periodic thread that does absolutely nothing except flashing
 * a LED.
 */
static THD_WORKING_AREA(waThread1, 128);
static THD_FUNCTION(Thread1, arg) {

  (void)arg;
  chRegSetThreadName("blinker");
  while (true) {
    palSetPad(GPIOD, GPIOD_LED4);       /* Orange.  */
    palClearPad(GPIOD, GPIOD_LED5);     /* Orange.  */
    chThdSleepMilliseconds(500);
    palClearPad(GPIOD, GPIOD_LED4);     /* Orange.  */
    palSetPad(GPIOD, GPIOD_LED5);       /* Orange.  */
    chThdSleepMilliseconds(500);
  }
}
void get_data(int id, char* buff, unsigned short* crc_extract);
unsigned int CRC16_2(char* buf, int len);
int GetString(char *s, int nmax);
void parse_data(int id, char* buff, unsigned long *data);
/*
 * Application entry point.
 */
int main(void) {

  /*
   * System initializations.
   * - HAL initialization, this also initializes the configured device drivers
   *   and performs the board-specific initializations.
   * - Kernel initialization, the main() function becomes a thread and the
   *   RTOS is active.
   */

  char buff[50];
  int sensor_id = 1;
  int crc_length;
  unsigned short crc_verify; // our crc value from our algorithm used to verify
  unsigned short crc_extract; // the extracted crc value from bms
  unsigned short temp_maxcell;
  unsigned short temp_mincell;
  unsigned short temp_temperature;
  unsigned long parsed_data;  // extracted data from the bms output once crc is verified
  float* temp_voltage; // variable to store voltage
  float* temp_current; // variable to store current


  halInit();
  chSysInit();

  /*
   * Activates the serial driver 2 using the driver default configuration.
   * PA2(TX) and PA3(RX) are routed to USAT2. Used to read/write to BMS
   */
  sdStart(&SD5, NULL);
  palSetPadMode(GPIOD, 2, PAL_MODE_ALTERNATE(8)); // sd5 tx
  palSetPadMode(GPIOC, 12, PAL_MODE_ALTERNATE(8)); // sd5 rx

  /*
   * Activates the serial driver 1 using the driver default configuration.
   * PB6(TX) and PB7(RX) are routed to USAT1. Used to read/write to Computer
   */

  sdStart(&SD1, NULL);
  palSetPadMode(GPIOA, 10, PAL_MODE_ALTERNATE(7)); //sd1 tx
  palSetPadMode(GPIOA, 9, PAL_MODE_ALTERNATE(7)); //sd1 rx


  canStart(&CAND1, &cancfg);
  palSetPadMode(GPIOB, 8, PAL_MODE_ALTERNATE(9)); // CAN1 rx
  palSetPadMode(GPIOB, 9, PAL_MODE_ALTERNATE(9)); // CAN1 tx


  /*
   * Starting the transmitter and receiver threads.
   */
  chThdCreateStatic(can_rx1_wa, sizeof(can_rx1_wa), NORMALPRIO + 7,
                    can_rx, (void *)&can1);
  chThdCreateStatic(can_tx_wa, sizeof(can_tx_wa), NORMALPRIO + 7,
                    can_tx, NULL);
  /*
   * Creates the example thread.
   */
  chThdCreateStatic(waThread1, sizeof(waThread1), NORMALPRIO, Thread1, NULL);

  /*
   * Normal main() thread activity, in this demo it does nothing except
   * sleeping in a loop and check the button state.
   */
  while (true) {
    /*if (palReadPad(GPIOA, GPIOA_BUTTON)) {*/

      ////// resets the sensor id and sets the crc_length
      if (sensor_id > 4)
      {
      sensor_id = 0;
      }

      if (sensor_id == 1 || sensor_id == 2){
        crc_length = 6;
      }
      else if(sensor_id == 3 || sensor_id ==4){
        crc_length = 4;
      }


      // Sending and Reading commands from TinyBMS
      get_data(sensor_id,buff, &crc_extract);

      // CRCing the input bytes
      crc_verify = CRC16_2(buff,crc_length);

      // If CRC is right then we extract and send the data
     if (crc_verify == crc_extract)
      {
       parse_data(sensor_id,buff,&parsed_data);
       //sdWrite(&SD1, &parsed_data, 4);
        if (sensor_id == 1)
         {
          temp_voltage = &parsed_data;
          if (*(temp_voltage) < MAX_VOLTAGE)
           {
            //sdWrite(&SD1, temp_voltage, 4);
            chprintf(&SD1, "The voltage is %f \r\n", *temp_voltage);
            chprintf(&SD1, "the integer voltage is %d \r\n",(int) (*(temp_voltage)*100));
            // data to send to the system to disconnect comes here

           }
         }
        else if (sensor_id == 2)
        {
          temp_current = &parsed_data;
          if (*(temp_current) < MAX_CURRENT)
          {
            chprintf(&SD1,"The current is %f \r\n", *temp_current);
            //sdWrite(&SD1, temp_current, 4);
          }
        }
        else if ( sensor_id == 3)
        {
          temp_maxcell = parsed_data;
          chprintf(&SD1,"The max cell voltage is %d \r\n", temp_maxcell);
          //sdWrite(&SD1, temp_maxcell, 2);

        }
        else if ( sensor_id == 4)
        {
          temp_mincell = parsed_data;
          chprintf(&SD1, "The min cell voltage is %d \r\n", temp_mincell);

        }
        else if ( sensor_id == 5)
        {
          temp_temperature = parsed_data;
          if (temp_temperature > MAX_TEMPERATURE)
          {
            chprintf(&SD1, "MAX_TEMP");
          }
        }
      }
     //what should happen if crc does something wrong

     //increment the sensor id to let it iterate
     sensor_id++;
    }

    chThdSleepMilliseconds(30000);
    halPolledDelay(15000);
    //sleep(10000);

  }
//}
  int GetString(char * s, int nmax)
  {
      unsigned char c;
      int n = 0;
      while(1)
      {

          if( (c=='\n') || (c=='\r') || n==(nmax-1) )
          {
              *s=0;
              return n;
          }
          else
          {
              *s=c;
              s++;
              n++;
          }
      }
  }

  unsigned int CRC16_2(char* buf, int len){
    unsigned int crc = 0xFFFF;
    unsigned int temp;
    for (int pos = 0; pos < len; pos++)
    {
    crc ^= (unsigned int)buf[pos];    // XOR byte into least sig. byte of crc

    for (int i = 8; i != 0; i--) {    // Loop over each bit
      if ((crc & 0x0001) != 0) {      // If the LSB is set
        crc >>= 1;                    // Shift right and XOR 0xA001
        crc ^= 0xA001;
      }
      else                            // Else LSB is not set
        crc >>= 1;                    // Just shift right
      }
    }
    return crc;
  }

void parse_data(int id, char* buff, unsigned long *data){
  int i;
  int lower_byte;
  int upper_byte;
  if (id == 1 || id == 2){
    lower_byte = 2;
    upper_byte = 5;
  }
  if (id ==3 || id ==4){
    lower_byte = 2;
    upper_byte = 3;
  }
  if (id == 5){
      lower_byte = 3;
      upper_byte = 8;
    }
  for (i = upper_byte; i >= lower_byte; i--){
      *data = *data <<8;
      *data = *data | buff[i];
         /*pack_voltage = pack_voltage | buff[4];
         pack_voltage = pack_voltage <<8;
         pack_voltage = pack_voltage | buff[3];
         pack_voltage = pack_voltage <<8;
         pack_voltage = pack_voltage | buff[2];
         */
     }
}

void get_data(int id, char* buff, unsigned short* crc_extract){
  char* temp_command;
  int read_size;
  int send_size;

  if (id == 1){
    temp_command = &get_pack_voltage;
    send_size = 4;
    read_size = 8;
  }
  if (id == 2){
    temp_command = &get_pack_current;
    send_size = 4;
    read_size = 8;
  }
  if (id == 3){
    temp_command = &get_max_voltage;
    send_size = 4;
    read_size = 6;
  }
  if (id == 4){
    temp_command = &get_min_voltage;
    send_size = 4;
    read_size = 6;
  }
  if (id == 5){
    temp_command = &get_temperature;
    send_size = 4;
    read_size = 11;
  }
  else


  sdWrite(&SD5, temp_command, send_size);
  sdReadTimeout(&SD5, buff, read_size, 100);
  //sdWrite(&SD1, buff, read_size);

  // Extracting CRC bytes from output to a variable
  *crc_extract = buff[read_size -1];
  *crc_extract = *crc_extract<<8;
  *crc_extract = *crc_extract | buff[read_size -2];


}

